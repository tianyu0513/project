<?php
$con=mysqli_connect("db.soic.indiana.edu","i308u18_team41","my+sql=i308u18_team41", "i308u18_team41");
// Check connection
if (mysqli_connect_errno())
{echo nl2br("Failed to connect to MySQL: " . mysqli_connect_error() . "\n "); }
else 
{ echo nl2br("Established Database Connection \n");}
$Section=mysqli_real_escape_string($con,$_POST["Section"]);
$sql="SELECT concat(st.name_f,' ',st.name_m,' ',st.name_l) as Fullname,
avg((case 
when (se.grade between 93 and 100) then 4.0
when (se.grade between 90 and 92.9) then 3.7
when (se.grade between 87 and 89.9) then 3.3
when (se.grade between 83 and 86.9) then 3.0
when (se.grade between 80 and 82.9) then 2.7
when (se.grade between 77 and 79.9) then 2.3
when (se.grade between 73 and 76.9) then 2.0
when (se.grade between 70 and 72.9) then 1.7
when (se.grade between 67 and 69.9) then 1.3
when (se.grade between 65 and 66.9) then 1.0
when (se.grade between 0 and 64.9) then 0.0
end)) as GPA
from student as st, Section_Enrolled as se
where st.studentid=se.student_id
and se.SectionNUM=$Section
group by Fullname
with rollup;
";
$result=mysqli_query($con,$sql);
if (mysqli_num_rows($result)>0){
	echo "<table border='1'>";
	echo"<tr>
	<th>Fullname</th>
	<th>GPA</th></tr>";
	while($row=mysqli_fetch_assoc($result)){
		echo"<tr><td>".$row["Fullname"]."</td>
		<td>".$row["GPA"]."</td></tr>";
	}
}
	else{
	echo "0 result";}
	
	mysqli_close($con);
?>