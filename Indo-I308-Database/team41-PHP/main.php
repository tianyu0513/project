<!doctype html>
<html>
<head> <h1> Final Project Team 41</h1> </head>
<body>

<h3>Query 1B </h3>
<p>A class roster for a *specified section* sorted by student’s last name, first name.
At the end, include the average grade (GPA for the class.)</p>
<form action="1b-sql.php" method="post">
Section:<select name="Section" required>
<option value=1>1</option>
<option value=2>2</option>
<option value=3>3</option>
<option value=4>3</option>
<option value=5>4</option>
<option value=6>5</option>
<option value=7>6</option>
<option value=8>7</option>
<option value=9>8</option>
<option value=10>9</option>
<option value=11>10</option>
<option value=12>12</option>
<option value=13>13</option>
<option value=14>14</option>
<option value=15>15</option>
<option value=16>16</option>
<option value=17>17</option>
<option value=18>18</option>
<option value=19>19</option>
<option value=20>20</option>
<option value=21>21</option>
<option value=22>22</option>
<option value=23>23</option>
<option value=24>24</option>
<option value=25>25</option>
<option value=26>26</option>
<option value=27>27</option>
<option value=28>28</option>
<option value=29>29</option>
<option value=30>30</option>
<option value=31>31</option>
<option value=32>32</option>
<option value=33>33</option>
<option value=34>34</option>
<option value=35>35</option>
<option value=36>36</option>
<option value=37>37</option>
<option value=38>38</option>
<option value=39>39</option>
<option value=40>40</option>
<option value=41>41</option>
<option value=42>42</option>
<option value=43>43</option>
<option value=44>44</option>
<option value=45>45</option>
<option value=46>46</option>
<option value=47>47</option>
<option value=48>48</option>
<option value=49>49</option>
<option value=50>50</option>
<option value=51>51</option>
<option value=52>52</option>
<option value=53>53</option>
<option value=54>54</option>
<option value=55>55</option>
<option value=56>56</option>
<option value=57>57</option>
<option value=58>58</option>
<option value=59>59</option>
<option value=60>60</option>
</select><br><br>

<input type="submit" name="Choose Section" value="Choose Section">
</form>



<h3>Query 2A </h3>
<p>Produce a list of rooms that are equipped with *some feature*—e.g., “wired instructor
station”.</p>
<form action="2a-sql.php" method="post">
Features:<select name="features" required>
<option value="In-Room Bathroom">In-Room Bathroom</option>
<option value="SmartBoard">SmartBoard</option>
<option value="Microphone">Microphone</option>
<option value="Ethernet WiFi">Ethernet WiFi</option>
<option value="wired instructor station">wired instructor station</option>
<option value="Group-Collab Whiteboards">Group-Collab Whiteboards</option>
</select><br><br>

<input type="submit" name="Choose Feature" value="Choose Feature">
</form>


<h3>Query 3A</h3>
<p> Produce a list of all faculty and all the courses they have ever taught. Show how many
times they have taught each course.</p>
<form action="3a-sql.php" method="post">

<input type="submit" name="Find Faculty Courses" value="Faculty Courses">
</form>

<h3>Query 5C</h3>
<p>A chronological list of all courses taken by a *specified student*. Show grades earned. Include overall hours earned and GPA at the end. (Hint: An F does not earn
hours.).</p>
<form action="5c-sql.php" method="post">
Student ID:<select name="Studentid" required>
<option value=1>1</option>
<option value=2>2</option>
<option value=3>3</option>
<option value=4>4</option>
<option value=5>5</option>
<option value=6>6</option>
<option value=7>7</option>
<option value=8>8</option>
<option value=9>9</option>
<option value=10>10</option>
<option value=11>11</option>
<option value=12>12</option>
<option value=13>13</option>
<option value=14>14</option>
<option value=15>15</option>
<option value=16>16</option>
<option value=17>17</option>
<option value=18>18</option>
<option value=19>19</option>
<option value=20>20</option>
<option value=21>21</option>
<option value=22>22</option>
<option value=23>23</option>
<option value=24>24</option>
<option value=24>24</option>
<option value=25>25</option>
<option value=26>26</option>
<option value=27>27</option>
<option value=28>28</option>
<option value=29>29</option>
<option value=30>30</option>
<option value=31>31</option>
<option value=32>32</option>
<option value=33>33</option>
</select><br><br>

<input type="submit" name="Choose Student" value="Choose Student">
</form>

<h3>Query 7A</h3>
<p>A roster of students who have a room in a *specific dorm”. Include room type.</p>
<form action="7a-sql.php" method="post">
Room Number:<select name="roomid" required>
<option value=1>1</option>
<option value=2>2</option>
<option value=3>3</option>
<option value=4>4</option>
<option value=5>5</option>
<option value=6>6</option>
<option value=7>7</option>
<option value=8>8</option>
<option value=9>9</option>
<option value=10>10</option>
<option value=11>11</option>
<option value=12>12</option>
<option value=13>13</option>
<option value=14>14</option>
<option value=15>15</option>
<option value=16>16</option>
<option value=17>17</option>
<option value=18>18</option>
</select><br><br>

<input type="submit" name="Choose Dorm Room" value="Choose Dorm Room">
</form>

<h3>Query 7B</h3>
<P>Produce a list of dorm room ‘anomalies’—single rooms occupied by more than one
student and double rooms occupied by a single student.</P>
<form action="7b-sql.php" method="post">

<input type="submit" name="submit" value="Find Dorm Anomalies">
</form>

<h3>Query 9A</h3>
<p>Produce a list of majors offered, along with the department that offers them and their
requirements to graduate (hours earned and overall GPA).</p>
<form action="9a-sql.php" method="post">

<input type="submit" name="Run Query" value="Run Query">
</form>

<h2> Additional Queries </h2>



<p>Produce a list of students who are eligible to register for a specified course.</p>
<form action="4b-sql.php" method="post">
Course:<select name="prereqid" required>
<option value=1>"Information Infrastructure II"</option>
<option value=2>"HCI/Interactive Design"</option>
<option value=3>"Capstone"</option>
<option value=4>"Introduction to Management II"</option>
<option value=5>"Computer in Business II"</option>
<option value=6>"Introduction to Marketing"</option>
<option value=7>"Finite II"</option>
<option value=8>"Non-Profit Accounting"</option>
<option value=9>"Health Care Systems"</option>
</select><br>

<input type="submit" name="Show Eligible Students" value="Show Eligible Students">
</form>
<br>
<p>Produces a list of rooms with their type of room, the Building name, and address. </p>
<form action="add2-sql.php" method="post">
Show Rooms:
<input type="submit" name="Find Room" value="Find Your Room">
</form>

</body>
</html>
