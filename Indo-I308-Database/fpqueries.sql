//2a
select r.roomid, r.features
from room as r
where r.features = "$features";

//3b
SELECT concat(f.f_name,' ',f.m_name,' ',f.l_name) as Fullname,c.Title
from Faculty as f, course as c, Section as s
where c.course_id=s.course_id
and s.FacultyID=f.FacultyID
and c.Title <> "Introduction to Informatics"
group by Fullname;

//1b
SELECT concat(st.name_f,' ',st.name_m,' ',st.name_l) as Fullname,
avg((case 
when (se.grade between 93 and 100) then 4.0
when (se.grade between 90 and 92.9) then 3.7
when (se.grade between 87 and 89.9) then 3.3
when (se.grade between 83 and 86.9) then 3.0
when (se.grade between 80 and 82.9) then 2.7
when (se.grade between 77 and 79.9) then 2.3
when (se.grade between 73 and 76.9) then 2.0
when (se.grade between 70 and 72.9) then 1.7
when (se.grade between 67 and 69.9) then 1.3
when (se.grade between 65 and 66.9) then 1.0
when (se.grade between 0 and 64.9) then 0.0
end)) as GPA
from student as st, Section_Enrolled as se
where st.studentid=se.student_id
and se.SectionNUM=2
group by Fullname
with rollup;

//7a 
select concat(st.name_f,' ',st.name_m,' ',st.name_l) as Fullname, d.roomid,d.type
from dorm as d, student as st, room as r
where d.roomid=st.roomid
and r.roomid=d.roomid
and r.type="dorm"
group by Fullname; 

//5c
select st.studentid,c.Title, 
(case
when (se.grade between 93 and 100) then "A"
when (se.grade between 90 and 92.9) then "B+"
when (se.grade between 87 and 89.9) then "B"
when (se.grade between 83 and 86.9) then "B-"
when (se.grade between 80 and 82.9) then "C+"
when (se.grade between 77 and 79.9) then "C"
when (se.grade between 73 and 76.9) then "C-"
when (se.grade between 70 and 72.9) then "D+"
when (se.grade between 67 and 69.9) then "D"
when (se.grade between 65 and 66.9) then "D-"
when (se.grade between 0 and 64.9) then "F"
END) AS Lettergrade,
(case
when (se.grade between 65 and 100) then 3
when (se.grade between 0 and 49) then 0
end) as creditearn
from student as st, Section_Enrolled as se,course as c,Section as s
where st.studentid=se.student_id
and se.sectionNUM=s.sectionNUM
and s.course_id=c.course_id
and st.studentid=1;



select st.studentid,c.Title
from student as st, Section_Enrolled as se,course as c,Section as s
where st.studentid=se.student_id
and se.sectionNUM=s.sectionNUM
and s.course_id=c.course_id
and st.studentid=1;




select c.Title, 
se.grade,
avg((case 
when (se.grade between 93 and 100) then 4.0
when (se.grade between 90 and 92.9) then 3.7
when (se.grade between 87 and 89.9) then 3.3
when (se.grade between 83 and 86.9) then 3.0
when (se.grade between 80 and 82.9) then 2.7
when (se.grade between 77 and 79.9) then 2.3
when (se.grade between 73 and 76.9) then 2.0
when (se.grade between 70 and 72.9) then 1.7
when (se.grade between 67 and 69.9) then 1.3
when (se.grade between 65 and 66.9) then 1.0
when (se.grade between 0 and 64.9) then 0.0
end)) as GPA,
sum((case
when (se.grade between 65 and 100) then 3
when (se.grade between 0 and 49) then 0
end)) as creditearned
from student as st, Section_Enrolled as se,course as c,Section as s
where st.studentid=se.student_id
and se.sectionNUM=s.sectionNUM
and s.course_id=c.course_id
and st.studentid=1
group by c.Title
with rollup;



SELECT concat(f.f_name,' ',f.m_name,' ',f.l_name) as Fullname,c.Title
from Faculty as f, course as c, Section as s
where c.course_id=s.course_id
and s.FacultyID=f.FacultyID
and c.Title <> "Introduction to Informatics"
group by Fullname;