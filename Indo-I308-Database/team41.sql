//1B.
SELECT concat(st.name_f,' ',st.name_m,' ',st.name_l) as Fullname,
avg((case 
when (se.grade between 93 and 100) then 4.0
when (se.grade between 90 and 92.9) then 3.7
when (se.grade between 87 and 89.9) then 3.3
when (se.grade between 83 and 86.9) then 3.0
when (se.grade between 80 and 82.9) then 2.7
when (se.grade between 77 and 79.9) then 2.3
when (se.grade between 73 and 76.9) then 2.0
when (se.grade between 70 and 72.9) then 1.7
when (se.grade between 67 and 69.9) then 1.3
when (se.grade between 65 and 66.9) then 1.0
when (se.grade between 0 and 64.9) then 0.0
end)) as GPA
from student as st, Section_Enrolled as se
where st.studentid=se.student_id
and se.SectionNUM=$Section
group by Fullname
with rollup;

//2A.
select r.roomid, r.features
from room as r
where r.features is Not NULL;

//3A
SELECT concat(f.f_name,' ',f.m_name,' ',f.l_name) as Fullname,c.title, f.FacultyID, COUNT(s.SectionNUM) as count
from Faculty as f, course as c, Section as s
WHERE c.course_id = s.course_id
AND s.course_id=c.course_id
AND f.FacultyID=s.FacultyID
GROUP BY c.title;

//5C
select c.Title, 
se.grade,
avg((case 
when (se.grade between 93 and 100) then 4.0
when (se.grade between 90 and 92.9) then 3.7
when (se.grade between 87 and 89.9) then 3.3
when (se.grade between 83 and 86.9) then 3.0
when (se.grade between 80 and 82.9) then 2.7
when (se.grade between 77 and 79.9) then 2.3
when (se.grade between 73 and 76.9) then 2.0
when (se.grade between 70 and 72.9) then 1.7
when (se.grade between 67 and 69.9) then 1.3
when (se.grade between 65 and 66.9) then 1.0
when (se.grade between 0 and 64.9) then 0.0
end)) as GPA,
sum((case
when (se.grade between 65 and 100) then 3
when (se.grade between 0 and 49) then 0
end)) as creditearned
from student as st, Section_Enrolled as se,course as c,Section as s
where st.studentid=se.student_id
and se.sectionNUM=s.sectionNUM
and s.course_id=c.course_id
and st.studentid=1
group by c.Title
with rollup;


//7A
select concat(st.name_f,' ',st.name_m,' ',st.name_l) as Fullname, d.roomid,d.type
from dorm as d, student as st, room as r
where d.roomid=st.roomid
and r.roomid=d.roomi
dand r.roomid=1
group by Fullname; 

//7B
SELECT d.roomid, d.type, COUNT(s.roomid) as Occupants
FROM dorm as d, student as s, room as r
WHERE d.roomid = r.roomid
AND r.roomid = s.roomid
GROUP BY d.roomid
HAVING (d.type = 'Single' AND Occupants > 1) OR (d.type = 'Double' AND Occupants < 2);

//9A
select m.name, d.dept_name, m.req_GPA,m.req_credits
FROM Major as m, Department as d
WHERE m.Dept_ID = d.Dept_ID
GROUP BY m.name;

//Additional Query

SELECT concat(st.name_f,' ',st.name_m,' ',st.name_l) as Fullname, pr.prereq_id , pr.course_id
FROM student as st, prereq as pr, Section as s, Section_Enrolled as se, course as c
WHERE st.studentid=se.student_id
AND se.SectionNUM=s.SectionNUM
AND s.course_id=pr.prereq_id
AND pr.prereq_id=c.course_id
AND se.student_id != pr.course_id
GROUP BY Fullname;

//Other Additional Query

select r.roomid as Room_Number, b.name as Building_Name, r.type, concat(b_street, ' ' ,b_city, ' ' , b_state, ' ', b_zip) as Address
FROM room as r, building as b
WHERE r.buildingid = b.buildingid;
